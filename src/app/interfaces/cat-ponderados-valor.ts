export default interface CatPonderadosValor {
    fiIdPonderadoValor: string,
    fcDescPonderadoValor: string,
    fcValorPonderadoValor: string,
    fiIdPonderadoEncabezado: string,
    persitenceId: number,
    persistenceVersion: any
}
