import CatPonderadosEncabezado from './cat-ponderados-encabezado';
import CatPonderadosValorVisual from './cat-ponderados-valor-visual';

export default interface CatPonderadosEncabezadoVisual {
    obj: CatPonderadosEncabezado,
    children: CatPonderadosValorVisual[],
    mouseEnter?: boolean,
    timeOutForDelete?: number
}
