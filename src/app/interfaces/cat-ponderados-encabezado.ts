export default interface CatPonderadosEncabezado {
    fiIdPonderadoEncabezado: string,
    fiIdTipoPonderado: string,
    fcDescPonderadoEncabezado: string,
    persitenceId: number,
    persistenceVersion: any
}
