import CatPonderadosValor from './cat-ponderados-valor';

export default interface CatPonderadosValorVisual {
    obj: CatPonderadosValor,
    beingEdited: boolean,
    mouseEnter?: boolean,
    timeOutForDelete?: number
}
